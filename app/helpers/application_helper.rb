module ApplicationHelper
  def view_link_icon(object)
    if current_user.try(:admin?)
      link_to "#{content_tag(:i,"",class:"glyphicon glyphicon-eye-open")}".html_safe, object ,class:"btn btn-info btn-simple btn-xs" ,rel:"tooltip", title:"View"
    end
  end
  def edit_link_icon(object)
    if current_user.try(:admin?)
      controller = "kkkkkk"
      if (controller_name == "suppliers/suppliers")
        controller = "suppliers/"
      end
      link_to "#{content_tag(:i,"",class:"glyphicon glyphicon-pencil")}".html_safe, "#{controller_name}/#{object.id}/edit" ,class:"btn btn-info btn-simple btn-xs" ,rel:"tooltip", title:"Edit"
    end
  end

  def edit_link_in_show(object)
    if current_user.try(:admin?)
      link_to "#{content_tag(:i,"",class:"glyphicon glyphicon-pencil")}".html_safe, "#{object.id}/edit" ,class:"btn btn-info btn-simple btn-xs" ,rel:"tooltip", title:"Edit"
    end
  end

  def delete_link_icon(object)
    if current_user.try(:admin?)
      link_to "#{content_tag(:i,"",class:"glyphicon glyphicon-trash")}".html_safe, object, method: :delete,data: {confirm: "Are you sure?"} ,class:"btn btn-info btn-simple btn-xs" ,rel:"tooltip", title:"Delete"
    end
  end

end
