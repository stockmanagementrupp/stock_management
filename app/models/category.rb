class Category < ApplicationRecord


  #Associations
  has_many :products

  #Validation
  validates :category_name,presence:true
end
