class Import < ApplicationRecord
  #Associations
  belongs_to :suppiler, optional: true
  belongs_to :user, optional: true
  has_many :products, inverse_of: :import
  accepts_nested_attributes_for :products, :reject_if => :all_blank, :allow_destroy => true
end
