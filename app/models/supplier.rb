class Supplier < ApplicationRecord

  #Associations
  has_many :import
  belongs_to :user,optional: true

  #Validation
  validates :supplier_name, presence: true
  validates :address, presence: true
end
