class SuppliersController < ApplicationController
  before_action :set_supplier, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user! , only: [:index,:show, :edit, :update ,:destroy, :new]
  before_action :admin_only , only: [:show, :edit, :update ,:destroy]
  # GET /suppliers
  # GET /suppliers.json
  def index
    @suppliers = Supplier.all
    @fields = ["ID","Supplier Name","Address"]
  end

  # GET /suppliers/1
  # GET /suppliers/1.json
  def show
    @fields = ["ID","Supplier Name","Address"]
    @a = controller_name
  end

  # GET /suppliers/new
  def new
    @supplier = current_user.suppliers.build
  end

  # GET /suppliers/1/edit
  def edit
  end

  # POST /suppliers
  # POST /suppliers.json
  def create
    @supplier = current_user.suppliers.build(supplier_params)

    respond_to do |format|
      if @supplier.save
        format.html { redirect_to @supplier, notice: 'Supplier was successfully created.' }
        format.json { render :show, status: :created, location: @supplier }
      else
        format.html { render :new }
        format.json { render json: @supplier.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /suppliers/1
  # PATCH/PUT /suppliers/1.json
  def update
    respond_to do |format|
      if @supplier.update(supplier_params)
        format.html { redirect_to @supplier, notice: 'Supplier was successfully updated.' }
        format.json { render :show, status: :ok, location: @supplier }
      else
        format.html { render :edit }
        format.json { render json: @supplier.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /suppliers/1
  # DELETE /suppliers/1.json
  def destroy
    @supplier.destroy
    respond_to do |format|
      format.html { redirect_to suppliers_url, notice: 'Supplier was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_supplier
      @supplier = Supplier.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def supplier_params
      params.require(:supplier).permit(:supplier_name, :image, :address)
    end

    # Admin Method
    def admin_only
      unless current_user.admin?
        redirect_to root_path
      end

    end
end
