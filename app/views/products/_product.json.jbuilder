json.extract! product, :id, :product_name, :product_qty, :unit_price_instock, :sale_unit_price, :category_id, :created_at, :updated_at
json.url product_url(product, format: :json)
