json.extract! sale, :id, :sale_date, :total, :created_at, :updated_at
json.url sale_url(sale, format: :json)
