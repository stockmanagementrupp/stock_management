json.extract! saledetail, :id, :sale_qty, :unit_price, :total, :created_at, :updated_at
json.url saledetail_url(saledetail, format: :json)
