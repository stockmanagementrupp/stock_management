json.extract! import, :id, :import_date, :supplier_id, :created_at, :updated_at
json.url import_url(import, format: :json)
