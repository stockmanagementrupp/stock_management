Rails.application.routes.draw do


  scope :admin do
    get "dashboard" => "dashboard#index"
    devise_for :users, :skip => [:registrations]
    resources :users
    resources :saledetails
      resources :sales
    resources :imports
    resources :products
    resources :categories
    resources :suppliers
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
    root "dashboard#index"
end
