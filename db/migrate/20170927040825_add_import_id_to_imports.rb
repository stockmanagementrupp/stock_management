class AddImportIdToImports < ActiveRecord::Migration[5.1]
  def change
    add_column :imports, :import_id, :integer
  end
end
