class CreateImports < ActiveRecord::Migration[5.1]
  def change
    create_table :imports do |t|
      t.datetime :import_date
      t.integer :supplier_id

      t.timestamps
    end
  end
end
