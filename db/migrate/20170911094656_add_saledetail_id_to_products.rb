class AddSaledetailIdToProducts < ActiveRecord::Migration[5.1]
  def change
    add_column :products, :sale_detail_id, :integer
  end
end
