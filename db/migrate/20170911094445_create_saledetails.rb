class CreateSaledetails < ActiveRecord::Migration[5.1]
  def change
    create_table :saledetails do |t|
      t.integer :sale_qty
      t.decimal :unit_price
      t.decimal :total

      t.timestamps
    end
  end
end
