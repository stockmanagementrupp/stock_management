class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string :product_name
      t.integer :product_qty
      t.decimal :unit_price_instock
      t.decimal :sale_unit_price
      t.integer :category_id
      t.string :product_code

      t.timestamps
    end
  end
end
